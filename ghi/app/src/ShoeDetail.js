import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
function ShoeDetail() {
    const { id } = useParams();
    const [details, setDetails] = useState("");


    // const getData = async () => {
    async function getData() {
        const response = await fetch('http://localhost:8080/api/shoes/' + id)
        console.log(response)

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setDetails(data)
        }
    }

    useEffect(() => {
        getData()
    }, [])

function displayImage(src, width,height) {
    var img = document.createElement("img");
    img.src = src;
    img.width = width;
    img.height = height;
    document.body.appendChild(img);
}



    return(
        <>
        <h1> { details.name }</h1>
        <p>manufacture: { details.manufacture }</p>
        <p>color: { details.color } </p>
        {/* <p>Location: { details.location.closet_name }</p> */}
        {/* <p>picture: {displayImage({details.picture_url}, 320, 250)}</p> */}
       </>
    )

}

export default ShoeDetail;

