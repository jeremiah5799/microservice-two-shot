import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function ShoesList() {
    const [shoes, setShoes] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/shoes/');

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setShoes(data.shoes)
        }
    }

    async function Delete(ShoeId) {
        const response = await fetch('http://localhost:8080' + ShoeId, {method: "DELETE"},)
        window.location.reload()
        console.log(response)
    }

    useEffect(()=>{
        getData()
    }, [])

    return (
        <table className="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Bin</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map(shoe => {
                    return (
                        <tr key={shoe.href}>
                            <td><Link to={shoe.href}>{ shoe.name }</Link></td>
                            <td>{ shoe.bin.name }<button onClick={() => { Delete(shoe.href) }}>Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default ShoesList
