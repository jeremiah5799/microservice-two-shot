import { useEffect, useState } from "react";
import { Link } from 'react-router-dom';



function HatsList() {
    const [hats, setHats] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/hats/')
        console.log(response)

        if(response.ok) {
            const data = await response.json();
            console.log(data)
            setHats(data.hats)
        }
    }

    async function Delete(HatId) {
        const response = await fetch('http://localhost:8090' + HatId, {method: "DELETE"},)
        window.location.reload()
        console.log(response)
    }

    useEffect(() => {
        getData()
    }, [])

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Style Name</th>
                    <th>Location</th>
                </tr>
            </thead>
            <tbody>
            {hats.map(hat => {
                    return (
                        <tr key={hat.href}>
                            <td><Link to={hat.href}>{hat.style_name }</Link></td>
                            <td>{ hat.closet_name }<button onClick={() => { Delete(hat.href) }}>Delete</button></td>
                        </tr>
                )
            })}
             </tbody>
        </table>
    );
}

export default HatsList;
