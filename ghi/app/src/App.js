import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './Shoes';
import CreateShoeForm from './ShoeForm';
import HatsList from './Hats';
import CreateHatForm from './HatForm';
import HatDetail from './HatDetail';
import ShoeDetail from './ShoeDetail';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route path="" element={<ShoesList />} />
            <Route path="create" element={<CreateShoeForm />} />
          </Route>
          <Route path="hats">
            <Route path="" element={<HatsList />} />
            <Route path="create" element={<CreateHatForm />} />
          </Route>
          <Route>
            <Route path="api/hats/:id" element={<HatDetail />} />
          </Route>
          <Route>
            <Route path="api/hats/:id" element={<HatDetail />} />
          </Route>
          <Route>
            <Route path="api/shoes/:id" element={<ShoeDetail />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
