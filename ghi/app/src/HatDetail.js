import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
function HatDetail() {
    const { id } = useParams();
    const [details, setDetails] = useState("");


    // const getData = async () => {
    async function getData() {
        const response = await fetch('http://localhost:8090/api/hats/' + id)
        console.log(response)
        
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setDetails(data)
        }
    }

    useEffect(() => {
        getData()
    }, [])

function displayImage(src, width,height) {
    var img = document.createElement("img");
    img.src = src;
    img.width = width;
    img.height = height;
    document.body.appendChild(img);
}

    

    return(
        <>
        <h1> { details.style_name }</h1>
        <p>fabric: { details.fabric }</p>
        <p>color: { details.color } </p>
        {/* <p>Location: { details.location.closet_name }</p> */}
        {/* <p>picture: {displayImage({details.picture_url}, 320, 250)}</p> */}
       </>
    )

}

export default HatDetail; 

