# Wardrobify

Team:

* Jeremiah - Hats
* Aidan - Shoes

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

Integrated a BinVO to poll Bin data from the Wardrobe microservice. Created a RESTful API to get a list of shoes, create a new shoe, and delete a shoe

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

Integrated a LocationVO to poll Location data from the Wardrobe microservice. Created a RESTful API to get a list of hats, create a new hat, and delete a hat.
