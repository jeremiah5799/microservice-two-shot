from django.contrib import admin
from django.urls import path
from .views import api_show_shoe, api_list_shoes

urlpatterns = [
    path('shoes/<int:id>/', api_show_shoe, name="api_show_shoe"),
    path('shoes/', api_list_shoes, name="api_list_shoes"),
]
