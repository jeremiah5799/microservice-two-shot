import json
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Shoe, BinVO
from django.http import JsonResponse
from common.json import ModelEncoder

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["name", "import_href"]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["name", "manufacture", "color", "picture_url"]
    encoders = {"bin": BinVODetailEncoder()}

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["name", "bin"]
    encoders = {"bin": BinVODetailEncoder()}

# Create your views here.
@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_shoe(request, id):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "bin" in content:
                bin = BinVO.objects.get(name=content["bin"])
                content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin"}
            )

        Shoe.objects.filter(id=id).update(**content)
        shoe = Shoe.objects.get(id=id)

        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
