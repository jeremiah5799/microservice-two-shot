from django.shortcuts import render
from .models import Hats, LocationVO
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json
from django.http import JsonResponse


class LocationVODetailEncoder(ModelEncoder):
    model= LocationVO
    properties=["closet_name", "import_href"]

class HatListEncoder(ModelEncoder):
    model= Hats
    properties=["style_name", "location"]
    encoders = {"location" : LocationVODetailEncoder()}
    
class HatDetailEncoder(ModelEncoder):
    model=Hats
    properties=["fabric", "style_name", "color", "picture_url", "location", "id"]
    encoders = {"location" : LocationVODetailEncoder()}

@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        hat = Hats.objects.all()
        return JsonResponse(
            {"hats":hat},
            encoder=HatListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            location_href = content['location']
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location"}, status = 400)
        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_hat(request, id):
    if request.method == "GET":
        hat = Hats.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = LocationVO.objects.get(closet_name=content["location"])
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse({"message": "Invalid location"}, status = 400)
        Hats.objects.filter(id=id).update(**content)
        hat = Hats.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Hats.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    
